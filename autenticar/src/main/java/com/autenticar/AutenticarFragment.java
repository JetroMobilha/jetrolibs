package com.autenticar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

@SuppressWarnings("unused")
public abstract class AutenticarFragment extends Fragment {

    protected Verifica mVerifica;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mVerifica = new Verifica(getActivity());
    }

    private EsperaDialogo esperaDialogo = new EsperaDialogo();

    @SuppressLint("StaticFieldLeak")
    protected class Tarefa extends AsyncTask<Object, Object, Object> {

        private boolean posesso = false;

        Tarefa() {}

        Tarefa(boolean posesso) {
            this.posesso = posesso;
        }

        public  void atualizar(Object o){
            onProgressUpdate(o);
        }

        @Override
        protected void onPreExecute() {
            anteDocarregamento();
        }

        @Override
        protected Object doInBackground(Object... params) {
            if (!posesso)
                return carregamentoDeDados(params[0]);
            else
                return carregamentoDeDados(params[0], this);
        }

        @Override
        protected void onPostExecute(Object o) {
            deposDocarregamento(o);
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            onProgressUpdateTarefa(values[0]);
        }
    }

    protected void onProgressUpdateTarefa(Object o) {
    }

    protected abstract void anteDocarregamento();

    protected abstract Object carregamentoDeDados(Object o);

    protected Object carregamentoDeDados(Object o, Tarefa tarefa) {
        return null;
    }

    protected abstract void deposDocarregamento(Object o);

    public void showDialogo() {
        try {
            if (esperaDialogo == null) {
                esperaDialogo = new EsperaDialogo();
            }
            esperaDialogo.show(getActivity().getSupportFragmentManager(), "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialogo(DialogFragment dialogFragment) {

        try {
            if (dialogFragment != null)
                dialogFragment.show(getActivity().getSupportFragmentManager(), "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void canselarDialogo() {
        try {
            if (esperaDialogo != null)
                esperaDialogo.dismissAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void canselarDialogo(DialogFragment dialogFragment) {

        try {
            if (dialogFragment != null)
                dialogFragment.dismissAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void iniciarTarefa(Object o) {
        new Tarefa().execute(o);
    }

    public void iniciarTarefaAtualizada(Object o) {
        new Tarefa(true).execute(o);
    }

    public Verifica getmVerifica() {
        return mVerifica;
    }

    public EsperaDialogo getEsperaDialogo() {
        return esperaDialogo;
    }
}
