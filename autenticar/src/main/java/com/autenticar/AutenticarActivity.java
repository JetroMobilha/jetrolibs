package com.autenticar;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

@SuppressWarnings("unused")
public abstract class AutenticarActivity extends AppCompatActivity {

    protected EsperaDialogo esperaDialogo = new EsperaDialogo();
    protected Verifica mVerifica;

    public AutenticarActivity(){
        mVerifica = new Verifica(this);
    }

    @SuppressLint("StaticFieldLeak")
    protected class Tarefa extends AsyncTask<Object, Object, Object> {

        private boolean posesso = false;

        Tarefa() {
        }

        Tarefa(boolean posesso) {
            this.posesso = posesso;
        }

        @Override
        protected void onPreExecute() {
            anteDocarregamento();
        }

        @Override
        protected Object doInBackground(Object... params) {
            if (!posesso)
                return carregamentoDeDados(params[0]);
            else
                return carregamentoDeDados(params[0], this);
        }
        @Override
        protected void onPostExecute(Object o) {
            deposDocarregamento(o);
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            onProgressUpdateTarefa(values);
        }
    }

    protected abstract void anteDocarregamento();

    protected abstract Object carregamentoDeDados(Object o);

    protected Object carregamentoDeDados(Object o,Tarefa tarefa) {
        return null;
    }

    protected abstract void deposDocarregamento(Object o);

    protected void onProgressUpdateTarefa(Object o){}

    public void showDialogo() {
        try {
            if (esperaDialogo == null) {
                esperaDialogo = new EsperaDialogo();
            }
            esperaDialogo.show(getSupportFragmentManager(), "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialogo(DialogFragment dialogFragment) {

        try {
            if (dialogFragment != null)
                dialogFragment.show(getSupportFragmentManager(), "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void canselarDialogo() {
        try {
            if (esperaDialogo != null)
                esperaDialogo.dismissAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void canselarDialogo(DialogFragment dialogFragment) {

        try {
            if (dialogFragment != null)
                dialogFragment.dismissAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void iniciarTarefa(Object o){
        new Tarefa().execute(o);
    }

    public void iniciarTarefaAtualizada(Object o) {
        new Tarefa(true).execute(o);
    }

    public Verifica getmVerifica() {
        return mVerifica;
    }

    public EsperaDialogo getEsperaDialogo() {
        return esperaDialogo;
    }
}
