package lib.jetrolibandroid;

import android.app.Application;

import com.imagens.Imagens;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Imagens.init(getApplicationContext(),"App lib text","App lib text tempo");
    }
}
