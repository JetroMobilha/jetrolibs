package lib.jetrolibandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.imagens.SalvarImagmAarelho;
import com.verimagens.Visualizadores.VerMidiaActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VerMidiaActivity.imagem(getApplicationContext(),String.valueOf(R.drawable.evento_chocolate));
    }
}
