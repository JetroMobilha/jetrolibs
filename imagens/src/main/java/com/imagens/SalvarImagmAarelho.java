package com.imagens;

import android.app.Application;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;



/**
 * Created by Jetro Mobilha on 11/11/2016.
 *
 */

 public  class SalvarImagmAarelho extends AsyncTask<Boolean,Void,Uri> {

    private String nome;
    Imagens imagens = Imagens.getInstance();

    public SalvarImagmAarelho(String nome){
        this.nome= nome;
    }

    @Override
    protected Uri doInBackground(Boolean... params) {

        return imagens.saveArrayToSDCard( nome , Imagens.getInstance().convertBitmapPraBytArray(imagens.getImagemBitmap(nome),100));
    }

    @Override
    protected void onPostExecute(Uri uri) {
        super.onPostExecute(uri);

        if (uri!=null ){
            Toast.makeText(imagens.getmContext().getApplicationContext(), R.string.imagem_guardada,Toast.LENGTH_SHORT).show();
            ImagemSalvaNotification.notify(imagens.getmContext(),uri);
        }else{

            Toast.makeText(imagens.getmContext().getApplicationContext(), R.string.imagem_n_guardada,Toast.LENGTH_SHORT).show();
        }
    }
}
