package com.imagens;

public interface ImagemInterface {

      boolean erroBaixado() ;

      void setClik(boolean clik);

      boolean isClik();
}
