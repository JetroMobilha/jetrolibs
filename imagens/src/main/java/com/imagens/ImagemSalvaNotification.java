package com.imagens;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class ImagemSalvaNotification {
    /**
     * The unique identifier for this type of notification.
     */
    private static final String NOTIFICATION_TAG = "ImagemSalva";
    private static final String NOTIFICATION_ID = "ImagemSalva_id";
    private static  int numero = 187;

    public static void notify(final Context context,
                              final Uri uri) {
        final Resources res = context.getResources();
        conte(); // contando o id da notificação

        final String ticker = res.getString(R.string.imagem_salva);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context,NOTIFICATION_ID)

                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setSmallIcon(R.drawable.ic_stat_imagem_salva)
                .setContentTitle(res.getString(R.string.imagem_salva))
                .setContentText(res.getString(R.string.imagem_salva))
                // Set ticker text (preview) information for this notification.
                .setTicker(ticker)
                // Show a number. This is useful when stacking notifications of
                // a single type.
                .setNumber(numero)

                // If this notification relates to a past or upcoming event, you
                // should set the relevant time information using the setWhen
                // method below. If this call is omitted, the notification's
                // timestamp will by set to the time at which it was shown.
                // TODO: Call setWhen if this notification relates to a past or
                // upcoming event. The sole argument to this method should be
                // the notification timestamp in milliseconds.
                //.setWhen(...)

                // Set the pending intent to be initiated when the user touches
                // the notification.
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                0,
                                new Intent(Intent.ACTION_VIEW,uri),
                                PendingIntent.FLAG_UPDATE_CURRENT))


                // Automatically dismiss the notification when it is touched.
                .setAutoCancel(true);

        notify(context, builder.build());
    }

    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (nm != null) {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_ID,NOTIFICATION_TAG,NotificationManager.IMPORTANCE_DEFAULT);
                nm.createNotificationChannel(channel);
            }

            nm.notify(NOTIFICATION_TAG, numero, notification);
        }
    }

    private static void conte(){
          numero = numero+1;
    }
}
