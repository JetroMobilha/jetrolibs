package com.jetro.views;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Formatter;
import java.util.List;

public class ViewVideo extends FrameLayout implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener
        , MediaPlayer.OnVideoSizeChangedListener, SurfaceHolder.Callback {

    private String TAG = "ViewVideo";
    private SurfaceView videoView;
    private ImageButton playPause;
    private TextView titulo, tempo;
    private ImageView son, expandir;
    private ImageView imagem;
    private Toolbar toolbar;
    private SeekBar progresso;
    private View view;
    private List<View> viewList;
    private String url;
    private ProgressBar progressBar;
    private int conte = 0, posisaoPlay = 0;
    private Handler handler = new Handler();

    private int INTERVALO_UP = 300;
    private Formatter mFormatter;

    private SurfaceHolder holder;
    private MediaPlayer mediaPlayer;

    private boolean mIsVideoSizeKnown = false;
    private boolean mIsVideoReadyToBePlayed = false;

    private int mVideoWidth;
    private int mVideoHeigth;

    private int posicao, duracao;


    private boolean play, gui, isSon = true, ok = false;

    public ViewVideo(Context context) {
        super(context);
        iniciar();
    }

    public ViewVideo(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        iniciar();
    }

    public ViewVideo(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        iniciar();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ViewVideo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        iniciar();
    }

    public void iniciar() {
        inflate(getContext(), R.layout.view_video, this);

        videoView = findViewById(R.id.video);
        playPause = findViewById(R.id.play);
        titulo = findViewById(R.id.titulo);
        tempo = findViewById(R.id.tempo);
        son = findViewById(R.id.vulume);
        expandir = findViewById(R.id.expandir);
        toolbar = findViewById(R.id.toolbar);
        progresso = findViewById(R.id.seekBar);
        imagem = findViewById(R.id.imagem);
        progressBar = findViewById(R.id.progressbar);

        holder = videoView.getHolder();
        holder.addCallback(this);

        setClickVideo(videoView);
        setClickPlay(playPause);
        setClickPlay(imagem);

        son.setOnClickListener(new SonListener());

        progresso.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) mediaPlayer.seekTo(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        videoView.setOnFocusChangeListener(new VideoFocusListener());

        if (isOk())
            inPlay();
        else
            inEspera();
        expandir.setVisibility(GONE);
    }

    public void inEspera() {
        setPlay(false);
        toolbar.setVisibility(GONE);
        progresso.setVisibility(GONE);
        tempo.setVisibility(GONE);
        // videoView.setVisibility(INVISIBLE);
        playPause.setVisibility(GONE);
        progressBar.setVisibility(GONE);
        imagem.setVisibility(VISIBLE);
        setOk(true);
        mosViewExter();

        if (progresso != null && mediaPlayer != null) {
            duracao = mediaPlayer.getDuration();
            progresso.setMax(duracao);
            progresso.setProgress(0);
        }
    }

    public void inProcesso() {
        setPlay(false);
        toolbar.setVisibility(GONE);
        progresso.setVisibility(GONE);
        tempo.setVisibility(GONE);
        // videoView.setVisibility(INVISIBLE);
        playPause.setVisibility(GONE);
        progressBar.setVisibility(VISIBLE);
        imagem.setVisibility(GONE);
        setOk(false);
        mosViewExter();
    }

    public void inPause() {
        setPlay(false);
        toolbar.setVisibility(GONE);
        progresso.setVisibility(GONE);
        tempo.setVisibility(GONE);
        // videoView.setVisibility(INVISIBLE);
        progressBar.setVisibility(GONE);
        playPause.setVisibility(VISIBLE);
        //imagem.setVisibility(VISIBLE);
        if (mediaPlayer != null && mediaPlayer.isPlaying()) mediaPlayer.pause();
        mosViewExter();
    }

    public void inPlay() {

        if (getUrl() != null) {
            setPlay(true);
            toolbar.setVisibility(VISIBLE);
            progresso.setVisibility(VISIBLE);
            tempo.setVisibility(VISIBLE);
            // videoView.setVisibility(VISIBLE);
            imagem.setVisibility(GONE);
            playPause.setVisibility(VISIBLE);
            progressBar.setVisibility(GONE);
            if (mediaPlayer != null && posicao > 0) {
                mediaPlayer.start();
                mediaPlayer.seekTo(posicao);
            } else if (mediaPlayer == null) {
                playVideo(url);
            } else
                startVideoPay();

            inicializarTarefasDeInfo();
            escViewExter();
            //  tempo();
        }
    }

    public void escondeGui() {
        setGui(false);
        toolbar.setVisibility(GONE);
        progresso.setVisibility(GONE);
        tempo.setVisibility(GONE);
        playPause.setVisibility(GONE);
    }

    public void mostrarGui() {
        setGui(true);
        toolbar.setVisibility(VISIBLE);
        progresso.setVisibility(VISIBLE);
        tempo.setVisibility(VISIBLE);
        playPause.setVisibility(VISIBLE);
    }

    public boolean isPlay() {
        return play;
    }

    public void setPlay(boolean play) {
        this.play = play;
    }

    public boolean isGui() {
        return gui;
    }

    public void setGui(boolean gui) {
        this.gui = gui;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public List<View> getViewList() {
        return viewList;
    }

    public void setViewList(List<View> viewList) {
        this.viewList = viewList;
    }

    public boolean isSon() {
        return isSon;
    }

    public void setSon(boolean son) {
        isSon = son;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private void setClickPlay(View view) {
        if (view != null)
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (url != null) {

                        if (isPlay()) {
                            inPause();
                            playPause.setImageResource(R.drawable.ic_play);
                        } else {
                            inPlay();
                            mostrarGui();
                            playPause.setImageResource(R.drawable.ic_pause);
                            videoView.callOnClick();
                        }
                    }
                }
            });
    }

    /**
     * Metodo usado para esconder a gui que sobrepoem a video
     *
     * @param view
     */
    private void setClickVideo(View view) {
        if (view != null)
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (conte == 0)
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                switch (conte) {
                                    case 2:
                                        if (isViewVisivel()) {
                                            escViewExter();
                                        } else {
                                            mosViewExter();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (mediaPlayer != null && mediaPlayer.isPlaying())
                                                        escViewExter();
                                                }
                                            }, 2000);
                                        }

                                        break;
                                    default:
                                        if (isPlay()) {
                                            if (isGui())
                                                escondeGui();
                                            else
                                                mostrarGui();
                                            escViewExter();
                                        } else {
                                            inPlay();
                                        }
                                }
                                conte = 0;
                            }
                        }, 500);

                    conte++;
                }
            });
    }

    public ImageView getExpandir() {
        expandir.setVisibility(VISIBLE);
        return expandir;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void mosViewExter() {
        if (getView() != null) getView().setVisibility(VISIBLE);

        if (getViewList() != null)
            for (View v : getViewList()) {
                v.setVisibility(VISIBLE);
            }
    }

    public boolean isViewVisivel() {
        boolean b = false;
        if (getView() != null) getView().setVisibility(VISIBLE);

        if (getViewList() != null)
            for (View v : getViewList()) {
                if (v.getVisibility() == VISIBLE)
                    b = true;
            }

        return (getView() != null && getView().getVisibility() == VISIBLE) || b;
    }

    public void escViewExter() {
        if (getView() != null) getView().setVisibility(GONE);

        if (getViewList() != null)
            for (View v : getViewList()) {
                v.setVisibility(GONE);
            }
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public void playVideo(String url) {

        // Construct a URI that points to the video resource that we want to play
        Uri videoUri = Uri.parse(url);

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDisplay(holder);
            mediaPlayer.setDataSource(getContext(), videoUri);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnVideoSizeChangedListener(this);
            inProcesso();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void inFocus() {
    }

    private void outFocus() {
        releaseMediaPlayer();
        doCleanUp();
    }

    private void startVideoPay() {
        Log.d(TAG, "startVideoPay called:");
        holder.setFixedSize(mVideoWidth, Math.max(mVideoHeigth, 700));
        mediaPlayer.start();
    }

    public void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void doCleanUp() {
        mVideoWidth = 0;
        mVideoHeigth = 0;
        mIsVideoReadyToBePlayed = false;
        mIsVideoSizeKnown = false;
        posicao = 0;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        Log.d(TAG, "onBufferingUpdate percent:" + i);
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingUpdate called");
        inPause();
        posicao = 0;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onPrepared called");
        mIsVideoReadyToBePlayed = true;
        if (mIsVideoSizeKnown)
            if (isOk()) inPlay();
            else inEspera();
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int width, int heigth) {
        Log.d(TAG, "onVideoSizeChanged called  width (" + width + ") or heigth (" + heigth + ")");
        if (width == 0 || heigth == 0) {
            Log.d(TAG, "invalid video width (" + width + ") or heigth (" + heigth + ")");
            return;
        }
        mIsVideoSizeKnown = true;
        mVideoWidth = width;
        mVideoHeigth = heigth;

        if (mIsVideoReadyToBePlayed)
            if (isOk()) inPlay();
            else inEspera();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.d(TAG, "surfaceCreated called");
        playVideo(url);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        Log.d(TAG, "surfaceChanged called");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.d(TAG, "surfaceDestroyed called");
        releaseMediaPlayer();
        doCleanUp();
    }

    private class SonListener implements OnClickListener {
        @Override
        public void onClick(View view) {
            try {
                AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
                if (isSon()) {
                    audioManager.adjustVolume(AudioManager.ADJUST_TOGGLE_MUTE, AudioManager.FLAG_VIBRATE);
                    setSon(false);
                    son.setImageResource(R.drawable.ic_volume_off);
                } else {
                    audioManager.adjustVolume(AudioManager.ADJUST_TOGGLE_MUTE, AudioManager.FLAG_PLAY_SOUND);
                    son.setImageResource(R.drawable.ic_volume_up);
                    setSon(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class VideoFocusListener implements OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean b) {
            Log.d(TAG, "onFocusChange called");
            if (!b) {
                inPause();
                outFocus();
            }
        }
    }

    private void inicializarTarefasDeInfo() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateProgressCallbackTask();
            }
        }, INTERVALO_UP);
    }

    private void updateProgressCallbackTask() {

        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            posicao = mediaPlayer.getCurrentPosition();
            progresso.setProgress(posicao);
            this.tempo.setText(stringToTime(duracao - posicao));
            if (isPlay()) inicializarTarefasDeInfo();
        }
    }

    /**
     * convert string to time
     *
     * @param timeMs time to be formatted
     * @return 00:00:00
     */
    private String stringToTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        mFormatter = new Formatter();

        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }
}
