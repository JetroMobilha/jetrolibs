package com.verimagens.Visualizadores.fragmentos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jetro.views.ViewVideo;
import com.verimagens.R;
import com.verimagens.Visualizadores.Interface.GetDado;

public class VisualisadorVideoFragmento extends BaseVisualisadorFragmento implements GetDado {

    protected static String IMAGEM = "imagem";

    public VisualisadorVideoFragmento() {
        // Required empty public constructor
    }

    public static VisualisadorVideoFragmento newInstance(@NonNull String imagem ) {
        VisualisadorVideoFragmento fragment = new VisualisadorVideoFragmento();
        Bundle bundle = new Bundle();
        bundle.putSerializable(IMAGEM,imagem);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //noinspection UnnecessaryLocalVariable
        View view =  inflater.inflate(R.layout.layout_video, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setLayout(view);
    }

    protected void setLayout(final View view){
        ViewVideo imageView = view.findViewById(R.id.imagem_visualizador_fragment);

        //noinspection ConstantConditions
        if (getArguments().getString(IMAGEM)!= null) {
            imageView.setOk(true);
            imageView.setUrl((getArguments().getString(IMAGEM)));

        }else {
            Toast.makeText(getContext(), R.string.sem_dados_para_mostar,Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }
    }

    @Override
    public String getDados() {
        if (getArguments() !=  null && getArguments().getString(IMAGEM) != null)
            return getArguments().getString(IMAGEM);
        else
            return null;
    }
}
