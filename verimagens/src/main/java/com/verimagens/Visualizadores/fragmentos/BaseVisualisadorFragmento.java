package com.verimagens.Visualizadores.fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.imagens.Imagens;
import com.verimagens.ClasseZoom;
import com.verimagens.R;
import com.verimagens.Visualizadores.Interface.ClickGui;
import com.verimagens.Visualizadores.Interface.GetDado;

public abstract class BaseVisualisadorFragmento extends Fragment implements GetDado {

    protected ClickGui clickGui;

    public BaseVisualisadorFragmento() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof  ClickGui) clickGui = (ClickGui) context;
    }
}
