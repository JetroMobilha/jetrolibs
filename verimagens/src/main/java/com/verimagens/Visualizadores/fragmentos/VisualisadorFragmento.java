package com.verimagens.Visualizadores.fragmentos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.imagens.Imagens;
import com.verimagens.ClasseZoom;
import com.verimagens.R;
import com.verimagens.Visualizadores.Interface.GetDado;

public class VisualisadorFragmento extends BaseVisualisadorFragmento implements GetDado {

    protected static String IMAGEM = "imagem";
    private ClasseZoom imageView;


    public VisualisadorFragmento() {
        // Required empty public constructor
    }

    public static VisualisadorFragmento newInstance(@NonNull String imagem) {
        VisualisadorFragmento fragment = new VisualisadorFragmento();
        Bundle bundle = new Bundle();
        bundle.putSerializable(IMAGEM, imagem);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.layout_imagem, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setLayout(view);
    }

    protected void setLayout(View view) {
        imageView = view.findViewById(R.id.imagem_visualizador_fragment);

        view.findViewById(R.id.imagem_visualizador_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if (clickGui != null)clickGui.clickGui();
            }
        });

        Imagens imagemsApp = Imagens.getInstance();
        //noinspection ConstantConditions
        if (getArguments().getString(IMAGEM) != null) {
            imagemsApp.lendoBitmapParaLista(getArguments().getString(IMAGEM), imageView);
        } else {
            Toast.makeText(getContext(), "Sem Imagem para mostrar", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public String getDados() {

        if (getArguments() !=  null && getArguments().getString(IMAGEM) != null)
            return getArguments().getString(IMAGEM);
        else
            return null;
    }

    public ClasseZoom getImageView() {
        return imageView;
    }
}
