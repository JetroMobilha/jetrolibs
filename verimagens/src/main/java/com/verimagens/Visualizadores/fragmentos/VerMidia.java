package com.verimagens.Visualizadores.fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.verimagens.R;
import com.verimagens.Visualizadores.Adptadores.VisualizadorDeImagemPagerAdapter;
import com.verimagens.Visualizadores.Adptadores.VisualizadorDeVIdeoPagerAdapter;
import com.verimagens.Visualizadores.Adptadores.VisualizadorPagerAdapter;
import com.verimagens.Visualizadores.Interface.OnEliminarObjeto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@SuppressWarnings("unused")
public class VerMidia extends Fragment {

    private static String LISTA = "lista";
    private static String LISTA_URI = "lista_uri";
    private static String POSICAO = "posicao";
    private static String IMAGEM = "imagem";
    private static String VIDEO = "video";
    private static String STRING = "string";
    private static String LISTENER = "listener";
    private FeicharListener mListenr;

    private int posicao = 0;

    private HashMap<String, Boolean> mVerToolbar = new HashMap<>();

    @Deprecated
    public static VerMidia imagem(@NonNull String imagem) {
        return  imagem(imagem,null);
    }

    @Deprecated
    public static VerMidia imagem(@NonNull String imagem, OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>();
        list.add(imagem);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(IMAGEM, true);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        bundle.putSerializable(LISTA, list);
        verFragmentos.setArguments(bundle);
        return verFragmentos;
    }

    @Deprecated
    public static VerMidia imagem(@NonNull List<String> imagem, OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>(imagem);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(IMAGEM, true);
        bundle.putSerializable(LISTA, list);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        verFragmentos.setArguments(bundle);
        return verFragmentos;
    }

    @Deprecated
    public static VerMidia imagem(@NonNull List<String> imagem) {
        return imagem(imagem,null);
    }

    @Deprecated
    public static VerMidia imagem(@NonNull List<String> imagem, int posicao, OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>(imagem);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LISTA, list);
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(IMAGEM, true);
        bundle.putInt(POSICAO, posicao);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        verFragmentos.setArguments(bundle);
        return verFragmentos;
    }

    @Deprecated
    public static VerMidia imagem(@NonNull List<String> imagem, int posicao) { return imagem(imagem,posicao,null);
    }

    @Deprecated
    public static VerMidia video(@NonNull String video, OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>();
        list.add(video);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LISTA, list);
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(VIDEO, true);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        verFragmentos.setArguments(bundle);
        return verFragmentos;
    }

    @Deprecated
    public static VerMidia video(@NonNull String video) {
        return video(video,null);
    }

    @Deprecated
    public static VerMidia video(@NonNull List<String> video) {
        return video(video,null);
    }

    @Deprecated
    public static VerMidia video(@NonNull List<String> video, OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>(video);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(VIDEO, true);
        bundle.putSerializable(LISTA, list);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        verFragmentos.setArguments(bundle);
        return verFragmentos;
    }

    public static void imagem(@NonNull FragmentManager fragmentManager, @NonNull String imagem ,OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>();
        list.add(imagem);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(IMAGEM, true);
        bundle.putSerializable(LISTA, list);
        verFragmentos.setArguments(bundle);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        mostrar(fragmentManager, verFragmentos);
    }

    public static void imagem(@NonNull FragmentManager fragmentManager, @NonNull String imagem) {
       imagem(fragmentManager,imagem,null);
    }

    public static void imagem(@NonNull FragmentManager fragmentManager, @NonNull List<String> imagem ) {
         imagem(fragmentManager,imagem,null);
    }

    public static void imagem(@NonNull FragmentManager fragmentManager, @NonNull List<String> imagem,OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>(imagem);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(IMAGEM, true);
        bundle.putSerializable(LISTA, list);
        verFragmentos.setArguments(bundle);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        mostrar(fragmentManager, verFragmentos);
    }

    public static void imagem(@NonNull FragmentManager fragmentManager, @NonNull List<String> imagem, int posicao,OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>(imagem);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LISTA, list);
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(IMAGEM, true);
        bundle.putInt(POSICAO, posicao);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        verFragmentos.setArguments(bundle);
        mostrar(fragmentManager, verFragmentos);
    }

    public static void imagem(@NonNull FragmentManager fragmentManager, @NonNull List<String> imagem, int posicao) {
        imagem(fragmentManager,imagem,posicao,null);
    }

    public static void video(@NonNull FragmentManager fragmentManager, @NonNull String video,OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>();
        list.add(video);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LISTA, list);
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(VIDEO, true);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        verFragmentos.setArguments(bundle);
        mostrar(fragmentManager, verFragmentos);
    }

    public static void video(@NonNull FragmentManager fragmentManager, @NonNull String video) {
        video(fragmentManager,video,null);
    }

    public static void video(@NonNull FragmentManager fragmentManager, @NonNull List<String> video,OnEliminarObjeto onEliminarObjeto) {
        ArrayList<String> list = new ArrayList<>(video);
        VerMidia verFragmentos = new VerMidia();
        Bundle bundle = new Bundle();
        bundle.putBoolean(STRING, true);
        bundle.putBoolean(VIDEO, true);
        bundle.putSerializable(LISTA, list);
        verFragmentos.setArguments(bundle);
        bundle.putSerializable(LISTENER,onEliminarObjeto);
        mostrar(fragmentManager, verFragmentos);
    }

    public static void video(@NonNull FragmentManager fragmentManager, @NonNull List<String> video) {
       video(fragmentManager,video,null);
    }

    public static void mostrar(@NonNull FragmentManager fragmentManager, Fragment fragment) {
        //noinspection ConstantConditions
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //especificando o tipo de animação para a entrada do fragmento
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.add(android.R.id.content, fragment, null);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public static void mostrar(@NonNull FragmentManager fragmentManager, Fragment fragment,int idConteiner) {
        //noinspection ConstantConditions
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //especificando o tipo de animação para a entrada do fragmento
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.add(idConteiner, fragment, null);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.visualisador_fragmento, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        final ViewPager viewPager = view.findViewById(R.id.visualizador_viewpager);
        VisualizadorPagerAdapter adapter = null;
        if (isString() && isImagem()) {
            //noinspection ConstantConditions,unchecked
            adapter = new VisualizadorDeImagemPagerAdapter(getActivity().getSupportFragmentManager(), (List<String>) getArguments().getSerializable(LISTA));
        } else if (isString() && isVideo()) {
            //noinspection ConstantConditions,unchecked
            adapter = new VisualizadorDeVIdeoPagerAdapter(getActivity().getSupportFragmentManager(), (List<String>) getArguments().getSerializable(LISTA));
        }


        viewPager.setAdapter(adapter);

        if (isPorPosicao()) {
            assert getArguments() != null;
            viewPager.setCurrentItem(getArguments().getInt(POSICAO));
            // definindo a posi;\ap para zero para não ser usado de novo
            getArguments().putInt(POSICAO,0);
        }

        view.findViewById(R.id.fechar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListenr != null)
                    mListenr.onFechar();
                else
                    getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        viewPager.addOnPageChangeListener(new OuvinteMudanca());

        View view1 = view.findViewById(R.id.eliminar);
        view1.setVisibility(View.GONE);

        if (isEliminador()){
            view1.setVisibility(View.VISIBLE);

            view1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        OnEliminarObjeto  objeto = (OnEliminarObjeto) getArguments().getSerializable(LISTENER);
                        VisualizadorPagerAdapter ad = (VisualizadorPagerAdapter) viewPager.getAdapter();
                        ad.getMlistaTitles().remove(posicao);
                        viewPager.removeViewAt(posicao);

                        if (objeto != null) objeto.onEliminarListenerAction(posicao);

                        if (ad.getCount() == 0 ) mListenr.onFechar();
                    }catch (Exception e){ e.printStackTrace(); }
                }
            });
        }


        // menu
        view.findViewById(R.id.rodar_esquerda).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        view.findViewById(R.id.rodar_directa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        view.findViewById(R.id.partilhar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        view.findViewById(R.id.baixar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FeicharListener)
            mListenr = (FeicharListener) context;
    }

    public boolean isPorPosicao() {
        return getArguments() != null && getArguments().getInt(POSICAO) != 0;
    }

    public boolean isString() {
        return getArguments() != null && getArguments().getBoolean(STRING);
    }

    public boolean isVideo() {
        return getArguments() != null && getArguments().getBoolean(VIDEO);
    }

    public boolean isImagem() {
        return getArguments() != null && getArguments().getBoolean(IMAGEM);
    }

    public boolean isEliminador() {
        return getArguments() != null && getArguments().getSerializable(LISTENER) != null && getArguments().getSerializable(LISTENER) instanceof OnEliminarObjeto ;
    }

    public interface FeicharListener {
        void onFechar();
    }

    public class OuvinteMudanca implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            posicao = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
