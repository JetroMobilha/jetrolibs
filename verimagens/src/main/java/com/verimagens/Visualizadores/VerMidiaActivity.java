package com.verimagens.Visualizadores;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.imagens.Imagens;
import com.imagens.SalvarImagmAarelho;
import com.verimagens.ApagarFicheiro;
import com.verimagens.ClasseZoom;
import com.verimagens.R;
import com.verimagens.Visualizadores.Adptadores.VisualizadorDeImagemPagerAdapter;
import com.verimagens.Visualizadores.Adptadores.VisualizadorDeVIdeoPagerAdapter;
import com.verimagens.Visualizadores.Adptadores.VisualizadorPagerAdapter;
import com.verimagens.Visualizadores.Interface.ClickGui;
import com.verimagens.Visualizadores.Interface.GetDado;
import com.verimagens.Visualizadores.Interface.OnEliminarObjeto;
import com.verimagens.Visualizadores.Interface.PartilhaDados;
import com.verimagens.Visualizadores.fragmentos.VisualisadorFragmento;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;


@SuppressWarnings("unused")
public class VerMidiaActivity extends AppCompatActivity implements ClickGui {

    private static String LISTA = "lista";
    private static String LISTA_URI = "lista_uri";
    private static String IMAGEM = "imagem";
    private static String VIDEO = "video";
    private static String STRING = "string";
    private static String LISTENER = "listener";
    private static String POSICAO = "posicao";

    private static String PARAMETRO = "bundle";

    private int posicao = 0;
    private Bundle bundle;
    private ViewPager viewPager;
    private boolean isGui = false;

    private VisualizadorPagerAdapter adapter ;

    private LinkedHashSet<FileTemporario> fileTemporarioList;


    private HashMap<String, Boolean> mVerToolbar = new HashMap<>();


    public static void imagem(Context context, @NonNull String imagem, OnEliminarObjeto onEliminarObjeto) {

        Intent intent = new Intent(context, VerMidiaActivity.class);
        ArrayList<String> list = new ArrayList<>();
        list.add(imagem);
        intent.putExtra(STRING, true);
        intent.putExtra(IMAGEM, true);
        intent.putExtra(LISTA, list);
        intent.putExtra(LISTENER, onEliminarObjeto);
        context.startActivity(intent);
    }

    public static void imagem(Context context, @NonNull String imagem) {
        imagem(context, imagem, null);
    }

    public static void imagem(Context context, @NonNull List<String> imagem, OnEliminarObjeto onEliminarObjeto) {

        Intent intent = new Intent(context, VerMidiaActivity.class);
        ArrayList<String> list = new ArrayList<>(imagem);
        intent.putExtra(STRING, true);
        intent.putExtra(IMAGEM, true);
        intent.putExtra(LISTA, list);
        intent.putExtra(LISTENER, onEliminarObjeto);
        context.startActivity(intent);
    }

    public static void imagem(Context context, @NonNull List<String> imagem) {
        imagem(context, imagem, null);
    }

    public static void imagem(Context context, @NonNull List<String> imagem, int posicao, OnEliminarObjeto onEliminarObjeto) {

        Intent intent = new Intent(context, VerMidiaActivity.class);
        ArrayList<String> list = new ArrayList<>(imagem);
        intent.putExtra(STRING, true);
        intent.putExtra(IMAGEM, true);
        String POSICAO = "posicao";
        intent.putExtra(POSICAO, posicao);
        intent.putExtra(LISTA, list);
        intent.putExtra(LISTENER, onEliminarObjeto);
        context.startActivity(intent);
    }

    public static void imagem(Context context, @NonNull List<String> imagem, int posicao) {
        imagem(context, imagem, posicao, null);
    }

    public static void video(Context context, @NonNull String video, OnEliminarObjeto onEliminarObjeto) {

        Intent intent = new Intent(context, VerMidiaActivity.class);
        ArrayList<String> list = new ArrayList<>();
        list.add(video);
        VerMidiaActivity verFragmentos = new VerMidiaActivity();
        Bundle bundle = new Bundle();
        intent.putExtra(LISTA, list);
        intent.putExtra(STRING, true);
        intent.putExtra(VIDEO, true);
        intent.putExtra(LISTENER, onEliminarObjeto);
        context.startActivity(intent);
    }

    public static void video(Context context, @NonNull String video) {
        video(context, video, null);
    }

    public static void video(Context context, @NonNull List<String> video, OnEliminarObjeto onEliminarObjeto) {
        Intent intent = new Intent(context, VerMidiaActivity.class);
        ArrayList<String> list = new ArrayList<>(video);
        VerMidiaActivity verFragmentos = new VerMidiaActivity();
        Bundle bundle = new Bundle();
        intent.putExtra(LISTA, list);
        intent.putExtra(STRING, true);
        intent.putExtra(VIDEO, true);
        intent.putExtra(LISTENER, onEliminarObjeto);
        context.startActivity(intent);
    }

    public static void video(Context context, @NonNull List<String> video) {
        video(context, video, null);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (bundle != null) outState.putParcelable(PARAMETRO, bundle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visualisador_fragmento);

        if (savedInstanceState == null && getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
        } else if (savedInstanceState != null) {
            bundle = savedInstanceState.getBundle(PARAMETRO);
        } else {
            finish();
        }

        fileTemporarioList = new LinkedHashSet<>();

        viewPager = findViewById(R.id.visualizador_viewpager);
        if (isString() && isImagem()) {
            //noinspection ConstantConditions,unchecked
            adapter = new VisualizadorDeImagemPagerAdapter(getSupportFragmentManager(), (List<String>) bundle.getSerializable(LISTA));
        } else if (isString() && isVideo()) {
            //noinspection ConstantConditions,unchecked
            adapter = new VisualizadorDeVIdeoPagerAdapter(getSupportFragmentManager(), (List<String>) bundle.getSerializable(LISTA));
        }

        viewPager.setAdapter(adapter);

        if (isPorPosicao()) {
            viewPager.setCurrentItem(bundle.getInt(POSICAO));
            // definindo a posição para zero para não ser usado de novo
            bundle.putInt(POSICAO, 0);
        }

        findViewById(R.id.fechar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        viewPager.addOnPageChangeListener(new OuvinteMudanca());

        View eliminar = findViewById(R.id.eliminar);
        eliminar.setVisibility(View.GONE);

        if (isEliminador()) {
            eliminar.setVisibility(View.VISIBLE);
            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        OnEliminarObjeto objeto = (OnEliminarObjeto) bundle.getSerializable(LISTENER);
                        VisualizadorPagerAdapter ad = (VisualizadorPagerAdapter) viewPager.getAdapter();
                        ad.getMlistaTitles().remove(posicao);
                        viewPager.removeViewAt(posicao);
                        if (objeto != null) objeto.onEliminarListenerAction(posicao);
                        if (ad.getCount() == 0) finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }


        // menu
        findViewById(R.id.rodar_esquerda).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rolarImagem(RotateDegrees.ROTATE_M90D.getValue());
            }
        });

        findViewById(R.id.rodar_directa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rolarImagem(RotateDegrees.ROTATE_90D.getValue());
            }
        });

        findViewById(R.id.partilhar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                partilhar();
            }
        });

        findViewById(R.id.baixar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baixarImagem();
            }
        });

        setMenu(View.GONE);
        if (isImagem()) {
            FileTemporario fileTemporario = new FileTemporario(getUrl());
            fileTemporario.execute();
            fileTemporarioList.add(fileTemporario);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            //noinspection unchecked
            startService(new Intent(getApplicationContext(), ApagarFicheiro.class));
            for (FileTemporario f : fileTemporarioList) {
                f.cancel(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isGui() {
        return isGui;
    }

    public void setGui(boolean gui) {
        isGui = gui;
    }

    public boolean isPorPosicao() {
        return bundle.getInt(POSICAO) != 0;
    }

    public boolean isString() {
        return bundle.getBoolean(STRING);
    }

    public boolean isVideo() {
        return bundle.getBoolean(VIDEO);
    }

    public boolean isImagem() {
        return bundle.getBoolean(IMAGEM);
    }

    public boolean isEliminador() {
        return bundle.getSerializable(LISTENER) != null && bundle.getSerializable(LISTENER) instanceof OnEliminarObjeto;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void clickGui() {
        if (isGui) {
            setGui(false);
            setMenu(View.GONE);
        } else {
            setGui(true);
            setMenu(View.VISIBLE);
        }
    }


    public void setMenu(int v) {
        findViewById(R.id.conteiner_gui).setVisibility(v);
    }

    public class OuvinteMudanca implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            posicao = position;

            String url = getUrl();
            if (mVerToolbar.get(url) == null || !mVerToolbar.get(url)) {
                FileTemporario fileTemporario = new FileTemporario(getUrl());
                fileTemporario.execute();
                fileTemporarioList.add(fileTemporario);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    public enum RotateDegrees {
        ROTATE_90D(90), ROTATE_180D(180), ROTATE_270D(270), ROTATE_M90D(-90), ROTATE_M180D(
                -180), ROTATE_M270D(-270);

        private final int VALUE;

        RotateDegrees(final int value) {
            this.VALUE = value;
        }

        public int getValue() {
            return VALUE;
        }
    }

    public void rolarImagem(int anglo) {
        try {
            ClasseZoom classeZoom =   ((VisualisadorFragmento) adapter.getMlistaFragmentos().get(viewPager.getCurrentItem())).getImageView();
            classeZoom.rodarView(anglo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void baixarImagem() {
        try {
            String urlImagem = getUrl();

            if (Imagens.getInstance().isExternalStorageWritable()) {
                new SalvarImagmAarelho(urlImagem).execute();
            } else {
                Toast.makeText(VerMidiaActivity.this, R.string.sem_permicao, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void partilhar() {

        if (getApplication() instanceof PartilhaDados)
            try {
                Imagens imagemsApp = Imagens.getInstance();
                String urlImagem = getUrl();

                File path = new File(imagemsApp.getCaminhoEmagemsTpm(urlImagem));

                if (path.exists()) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(getApplicationContext(),
                            ((PartilhaDados) getApplication()).gettAutorFileProvides() // chamdo apartir da classe concreta que executar esta clase
                            , new File(imagemsApp.getCaminhoEmagemsTpm(urlImagem))));

                    intent.setType("image/*");
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public String getUrl() {
        GetDado getDado = (GetDado) ((VisualizadorDeImagemPagerAdapter) viewPager.getAdapter()).getMlistaFragmentos().get(viewPager.getCurrentItem());
        return getDado.getDados();
    }

    @SuppressLint("StaticFieldLeak")
    private class FileTemporario extends AsyncTask<Void, Void, Boolean> implements Serializable {

        private String mFarras;

        FileTemporario(String farras) {
            this.mFarras = farras;
        }

        @Override
        protected void onPreExecute() {

            findViewById(R.id.partilhar).setVisibility(View.GONE);
            findViewById(R.id.baixar).setVisibility(View.GONE);
            if (mVerToolbar.get(mFarras) == null) mVerToolbar.put(mFarras, false);
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            String objetoFarras = getUrl();
            mVerToolbar.put(mFarras, aVoid);
            if (aVoid && objetoFarras.equals(mFarras)) {
                findViewById(R.id.partilhar).setVisibility(View.VISIBLE);
                findViewById(R.id.baixar).setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            Imagens imagemsApp = Imagens.getInstance();
            File file = new File(imagemsApp.getCaminhoEmagems(mFarras));
            File file2 = new File(imagemsApp.getCaminhoEmagemsTpm(mFarras));

            //noinspection SimplifiableIfStatement
            if (file.exists()) {
                return true;
            } else if (file2.exists()) {
                return true;
            } else {
                Bitmap bitmap = imagemsApp.getImagemBitmap(mFarras);
                if (bitmap != null) {
                    return imagemsApp.saveArrayToInternalStorageImagems(mFarras
                            , imagemsApp.getCaminhoEmagemsTpm(), imagemsApp.convertBitmapPraBytArray(bitmap, 100));
                } else {
                    return false;
                }
            }
        }
    }
}
