package com.verimagens;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MatrixRotateScaleSkewTranslateActivity extends AppCompatActivity {

    // Save current rotate degree.
    private float currRotateDegree = 0;

    // Save current x scale.
    private float currXScale = 1;
    private float currYScale = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity);

        final ImageView imageViewOriginal = (ImageView)findViewById(R.id.partilhar);
        BitmapDrawable originalBitmapDrawable = (BitmapDrawable) imageViewOriginal.getDrawable();
        final Bitmap originalBitmap = originalBitmapDrawable.getBitmap();
        final int originalImageWith = originalBitmap.getWidth();
        final int originalImageHeight = originalBitmap.getHeight();
        final Config originalImageConfig = originalBitmap.getConfig();

        /* Matrix rotate example.*/
        Button buttonRotate = (Button)findViewById(R.id.partilhar);
        buttonRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currRotateDegree += 60;
                this.rotateImage(currRotateDegree);
            }

            private void rotateImage(float rotateDegree)
            {
                // Create a bitmap which has same width and height value of original bitmap.
                Bitmap rotateBitmap = Bitmap.createBitmap(originalImageWith, originalImageHeight, originalImageConfig);

                Canvas rotateCanvas = new Canvas(rotateBitmap);

                Matrix rotateMatrix = new Matrix();

                // Rotate around the center point of the original image.
                rotateMatrix.setRotate(rotateDegree, originalBitmap.getWidth()/2, originalBitmap.getHeight()/2);

                Paint paint = new Paint();
                rotateCanvas.drawBitmap(originalBitmap, rotateMatrix, paint);
                imageViewOriginal.setImageBitmap(rotateBitmap);
            }
        });

        /* Matrix scale example. */
        Button buttonScale = (Button)findViewById(R.id.partilhar);
        buttonScale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currXScale -= 0.1;
                currYScale -= 0.1;

                if(currXScale <= 0)
                {
                    currXScale = 1;
                }

                if(currYScale <= 0)
                {
                    currYScale = 1;
                }
                this.scaleImage(currXScale, currYScale);
            }

            private void scaleImage(float xScale, float yScale)
            {
                // Create a bitmap which has scaled width and height value from original bitmap.
                Bitmap scaleBitmap = Bitmap.createBitmap((int)(originalImageWith * xScale), (int)(originalImageHeight * yScale), originalImageConfig);

                Canvas scaleCanvas = new Canvas(scaleBitmap);

                Matrix scaleMatrix = new Matrix();

                // Set x y scale value.
                scaleMatrix.setScale(xScale, yScale, originalImageWith/2, originalImageHeight/2);

                Paint paint = new Paint();
                scaleCanvas.drawBitmap(originalBitmap, scaleMatrix, paint);
                imageViewOriginal.setImageBitmap(scaleBitmap);
            }
        });

        /* Matrix skew example. */
        Button buttonSkew = (Button)findViewById(R.id.partilhar);
        buttonSkew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Generate a random skew value.
                double randomNumber = Math.random();

                double xSkew = randomNumber;
                double ySkew = randomNumber;
                this.skewImage(xSkew, ySkew);
            }

            private void skewImage(double xSkew, double ySkew)
            {
                // According to the skew ratio of the picture, calculate the size of the image after the transformation.
                int xAfterSkew = (int)(originalImageWith * (1 + xSkew));
                int yAfterSkew = (int)(originalImageHeight * (1 + ySkew));

                Bitmap skewBitmap = Bitmap.createBitmap(xAfterSkew, yAfterSkew, originalImageConfig);

                Canvas skewCanvas = new Canvas(skewBitmap);

                Matrix skewMatrix = new Matrix();

                // Set x y skew value.
                skewMatrix.setSkew((float)xSkew, (float)ySkew, originalImageWith/2, originalImageHeight/2);

                Paint paint = new Paint();
                skewCanvas.drawBitmap(originalBitmap, skewMatrix, paint);
                imageViewOriginal.setImageBitmap(skewBitmap);
            }
        });

        /* Matrix translate example. */
        Button buttonTranslate = (Button)findViewById(R.id.partilhar);
        buttonTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Generate a random translate value.
                Random random = new Random();
                int xTranslate = random.nextInt(1000);
                int yTranslate = random.nextInt(2000);
                this.translateImage(xTranslate, yTranslate);
            }

            private void translateImage(int xTranslate, int yTranslate)
            {
                // According to the skew ratio of the picture, calculate the size of the image after the transformation.
                Bitmap translateBitmap = Bitmap.createBitmap(originalImageWith + xTranslate, originalImageHeight + yTranslate, originalImageConfig);

                Canvas translateCanvas = new Canvas(translateBitmap);

                Matrix translateMatrix = new Matrix();

                // Set x y translate value..
                translateMatrix.setTranslate(xTranslate, yTranslate);

                Paint paint = new Paint();
                translateCanvas.drawBitmap(originalBitmap, translateMatrix, paint);
                imageViewOriginal.setImageBitmap(translateBitmap);
            }
        });
    }
}
