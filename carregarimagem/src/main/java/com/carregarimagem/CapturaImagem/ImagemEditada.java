package com.carregarimagem.CapturaImagem;

import android.graphics.Bitmap;

/**
 * Created by Jetro Domigos on 07/11/2017.
 *
 */

public interface ImagemEditada  {
    void onImagemEditada(Bitmap bitmap);
}


