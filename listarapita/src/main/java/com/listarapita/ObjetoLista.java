package com.listarapita;

/**
 *
 */
public interface ObjetoLista {
    int getItemId();
    int getItemIdServedor();
    String getItemCodigo();
}
